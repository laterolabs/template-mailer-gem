# TemplateMailer

Used to send email that has been templated in an upstream platform (Sendgrid). Can help seperate organizational teams by allowing marketers access to controlling the layout and design of the emails without taking up development resources.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'template_mailer', '~> 0.1'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install template_mailer

## Usage
### Configuration
***
There are two ways for configuration:

1. **Standalone**
    ```ruby
    TemplateMailer.configure do |config|
      ...
    end
    ```

2. **Rails**
    ```ruby
      config.template_mailer...
    ```
    The following options are allowed with their associated defaults:
    ```ruby
      config.template_mailer.platform = :sendgrid # The upstream platform to send templates from
      config.template_mailer.deliver_later_queue_name = :mailers # The name of the queue to use in your ActiveJob
      config.template_mailer.platform.default_mailer_options[:from] = nil # The from address to use to send out emails
    ```
### Creating a Serializer
***
1. Create a new file structure app/serializers/mailer_templates
2. Within this folder define any objects you want to be able to serialize
```ruby
# User here must be of the duck type to the object you will pass into TemplateMailer::Mail.new(objects_to_serialize)
class UserTemplateSerializer < TemplateMailer::ObjectSerializer
  serializable_fields 'string', :symbol, :foo

  # You can define any methods here that will also be serialized out (note foo in the array of serializable fields)
  def foo
    :bar
  end
end
```

### Creating an Email Template
***
You can now begin crafting template emails to send out to the platform of choice that you selected in the configuration step!

```ruby
  template = TemplateMailer::Mail.new(foo, bar, to: 'email@email.ca', template: :template_name)
  template.deliver # Will be delivered immediately
  template.deliver_later # Will be delivered in a background job in the mailer we configured before (deliver_later_queue_name)
```

See code for YARD Documentation of this method

## Development

After checking out the repo, run `bundle install` to install dependencies.
Run `bundle exec rspec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## Contributing

Contributions are welcome to all Latero members on Bitbucket!

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
