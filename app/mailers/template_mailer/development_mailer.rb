class TemplateMailer::DevelopmentMailer < ApplicationMailer

  def dummy_email(to, from, subject, html)
    @html = html
    mail(to: to, from: from, subject: subject)
  end

end