class TemplateMailer::Template < ActiveRecord::Base #:nodoc:
  validates_format_of :test_eval, without: /(save|delete|destroy|update)/

  def send_test(email)
    return false if test_eval.blank?

    EmailJob.perform_later(email, eval(test_eval), self)
  end

  def sendgrid_template_name
    return if sendgrid_template_id.blank?

    response = self.class.sendgrid_api.client.templates._(sendgrid_template_id).get()
    JSON.parse(response.body)["name"]
  end

  def self.sendgrid_api
    @@sendgrid_api ||= SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
  end

  def self.method_missing(message, *args, &block)
    if message.to_s.ends_with?('_template')
      find_or_create_by(key: message.to_s.gsub(/_template\z/, ''))
    else
      super
    end
  end
end
