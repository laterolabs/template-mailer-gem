class CreateTemplateMailerTables < ActiveRecord::Migration[6.1]
  def change
    create_table :template_mailer_templates do |t|
      t.string :key
      t.string :template_id
      t.string :test_eval

      t.timestamps
    end
  end
end
