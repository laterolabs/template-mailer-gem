require 'active_job'
require 'active_record'
require 'active_support/all'
require 'sendgrid-ruby'
require 'ruby-handlebars'

require 'template_mailer/version'
require 'template_mailer/engine' if defined?(Rails::Engine)

module TemplateMailer
  extend ActiveSupport::Autoload

  autoload :EmailSerializer
  autoload :EmailTemplateJob
  autoload :Mail
  autoload :ObjectSerializer
  autoload :SerializerHelper

  mattr_accessor :platform, instance_accessor: false, default: :sendgrid
  mattr_accessor :default_mailer_options, instance_accessor: false, default: ActiveSupport::OrderedOptions.new
  mattr_accessor :deliver_later_queue_name, instance_accessor: false, default: :mailers
  mattr_accessor :use_development_mailer, instance_accessor: false, default: false
  module Services
    autoload :BaseService, 'template_mailer/services/base_service'
    autoload :SendGridService, 'template_mailer/services/sendgrid_service'
  end

  if defined?(Rails)
    def self.configure(&block)
      if block_given?
        block.call(Railtie.config.template_mailer)
      else
        Railtie.config.template_mailer
      end
    end
  else
    def self.configure
      yield self
    end
  end
end
