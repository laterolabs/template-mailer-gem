module TemplateMailer
  class EmailSerializer
    include SerializerHelper

    attr_accessor :object, :objects, :result

    def initialize(object)
      @result = object if object.kind_of?(Hash) and return
      @object = object
      @objects = Array(object)
      @object.is_a?(ActiveRecord::Relation) || @object.is_a?(Array) ? map_many : map_single
    end

    def count
      objects.size
    end

    def to_h
      @result
    end

    private

    def map_single
      @result = HashWithIndifferentAccess.new
      result[object.class.name.underscore] = serializer.new(object).to_h
    end

    def map_many
      @result = HashWithIndifferentAccess.new { |hash, key| hash[key] = [] }

      objects.each do |object|
        result[object.class.table_name] << serializer.new(object).to_h
      end
    end
  end
end
