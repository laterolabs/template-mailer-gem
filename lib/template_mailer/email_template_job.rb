module TemplateMailer
  class EmailTemplateJob < ActiveJob::Base
    queue_as { TemplateMailer.deliver_later_queue_name }

    def perform(data, klass, to:, id:)
      template = klass.constantize.new(data, to: to, id: id)
      template.deliver_now
    end
  end
end
