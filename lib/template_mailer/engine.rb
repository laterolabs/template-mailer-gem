module TemplateMailer
  class Engine < Rails::Engine
    isolate_namespace TemplateMailer

    config.template_mailer = ActiveSupport::OrderedOptions.new
    config.template_mailer.default_mailer_options = ActiveSupport::OrderedOptions.new

    initializer 'template_mailer.configs' do
      config.after_initialize do |app|
        TemplateMailer.deliver_later_queue_name = app.config.template_mailer.deliver_later_queue_name || :mailers
        TemplateMailer.default_mailer_options   = app.config.template_mailer.default_mailer_options || {}
        TemplateMailer.platform                 = app.config.template_mailer.platform || :sendgrid
        TemplateMailer.use_development_mailer   = app.config.template_mailer.use_development_mailer || false
      end
    end
  end
end
