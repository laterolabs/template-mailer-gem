#
# Use to create the main mail message to deliver to the upstream platform.
#
# @param [<Object>] *objects Splat number of serializable objects
# @param [String] to Address email is being sent to
# @param [Symbol] template Template key to use
#
# @example
# TemplateMailer::Mail.new(foo, bar, to: 'email@email.ca', template: :example_template)
#
# @note
#   The template value has 2 effects. It will first try and find an existing TemplateMailer::Template
#   record in the db table and use the attatched #template_id to use to send the template. If there is no
#   template then a new record will be saved indicating that a new template needs to be created
#   upstream and set on the record. This is the case for any record without a #template_id
#
module TemplateMailer
  class Mail
    attr_reader :objects, :to, :template

    def initialize(*objects, to:, template:)
      @objects = objects.map do |object|
        object.respond_to?(:to_h) ? object : EmailSerializer.new(object)
      end
      @to = to
      @template = defined?(Template) ? Template.send(template) : template
    end

    def deliver_now
      service.send_template(self)
    end
    alias deliver deliver_now

    def deliver_later(options = {})
      EmailTemplateJob.set(options).perform_later(serialize, self.class.name, to: to, id: template)
    end

    def serialize
      objects.inject(HashWithIndifferentAccess.new) { |h, object| h.merge(object.to_h) }
    end

    private

    def service
      @service ||= case TemplateMailer.platform
                   when Services::BaseService::SENDGRID then Services::SendGridService.new
                   end
    end
  end
end
