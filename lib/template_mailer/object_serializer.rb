module TemplateMailer
  class ObjectSerializer
    include SerializerHelper

    attr_accessor :object

    class << self
      attr_reader :fields

      #
      # Sets serializable field names
      #
      # @param [Array<String, Symbol>] *field_names fields to serialize
      #
      # @return [void]
      #
      def serializable_fields(*field_names)
        @fields = field_names
      end
    end

    def initialize(object = nil)
      @object = object
    end

    #
    # Serializes Object
    #
    # @return [Hash{String => Value}] <description>
    #
    def to_h
      return nil if object.nil?

      self.class.fields.map { |f| [f, value_or_serialized(f)] }.to_h
    end

    private

    def value_or_serialized(method_name)
      result = respond_to?(method_name) ? send(method_name) : object.send(method_name)
      if result.is_a?(ActiveRecord::Relation)
        result.map { |r| extract_value_or_serialized(r) }
      else
        extract_value_or_serialized(result)
      end
    end

    def extract_value_or_serialized(value)
      serializer.new(value).to_h
    rescue NameError
      case value
      when Time
        value.iso8601
      else
        value
      end
    end
  end
end
