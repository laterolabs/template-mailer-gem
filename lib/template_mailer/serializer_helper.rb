module TemplateMailer
  module SerializerHelper
    extend ActiveSupport::Concern

    included do
      #
      # <Description>
      #   if you have defined a serializer under MailerTemplates::<Object>TemplateSerializer < TemplateMailer::ObjectSerializer
      #   then this method will return
      #
      # @return [ObjectSerializer] returns the mail serializer for <Object>
      # @example
      #   class UserTemplateSerializer < TemplateMailer::ObjectSerializer
      #     serializable_fields :organization_id
      #   end
      #
      #   class Example
      #     include SerializerHelper
      #     attr_reader :object
      #   end
      #
      def serializer
        "MailerTemplates::#{object.class.name}TemplateSerializer".constantize
      end
    end
  end
end
