# Abstract class that needs a concrete implementation
module TemplateMailer
  module Services
    class BaseService
      class InvalidRequest < StandardError; end

      SUPPORTED_SERVICES = [
        SENDGRID = :sendgrid
      ]

      def send_template(mail)
        raise NotImplementedError
      end
    end
  end
end
