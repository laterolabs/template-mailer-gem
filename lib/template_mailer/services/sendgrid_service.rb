module TemplateMailer
  module Services
    class SendGridService < BaseService
      include SendGrid

      attr_reader :client

      def initialize
        @client = ::SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY']).client
      end

      def send_template(mail)
        m = SendGrid::Mail.new
        m.template_id = mail.template.template_id
        m.from = Email.new(email: TemplateMailer.default_mailer_options.from)
        personalization = Personalization.new
        personalization.add_to Email.new(email: mail.to)
        personalization.add_dynamic_template_data(mail.serialize)
        m.add_personalization(personalization)
        if TemplateMailer.use_development_mailer
          _send_development_email(mail)
        else
          handle_response client.mail._('send').post(request_body: m.to_json)
        end
      end

      def _send_development_email(mail)
        template = JSON.parse(client.templates._("#{mail.template.template_id}").get.body)
        active_version = template["versions"].detect {|h| h["active"] == 1}
        html_body = Handlebars::Handlebars.new.compile(active_version["html_content"]).call(mail.serialize)
        TemplateMailer::DevelopmentMailer.dummy_email(mail.to, TemplateMailer::default_mailer_options.from, active_version["subject"], html_body).deliver
      end

      private

      def handle_response(response)
        raise InvalidRequest.new(response.parsed_body) if response.status_code.to_i != 202
      end
    end
  end
end
