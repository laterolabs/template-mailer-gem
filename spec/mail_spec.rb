require 'spec_helper'

RSpec.describe TemplateMailer::Mail do
  let(:double) { instance_double('Obj', foo: :bar) }
  let(:serializer) { TemplateMailer::ObjectSerializer.new(double) }
  let(:to) { 'example@example.com' }
  let(:template) { OpenStruct.new(template_id: :template) }

  subject { described_class.new(double, to: to, template: template) }

  context 'sendgrid' do
    before do
      TemplateMailer::ObjectSerializer.serializable_fields :foo
      TemplateMailer.platform = TemplateMailer::Services::BaseService::SENDGRID
      TemplateMailer.default_mailer_options[:from] = 'allen@laterolabs.com'
      allow(TemplateMailer::EmailSerializer).to receive(:new).and_return(serializer)
    end

    describe '#deliver_now' do
      context 'valid' do
        it 'sends the template' do
          stub_request(:post, sendgrid_uri_for_webmock).to_return(status: 202)
          expect(subject.deliver_now)
        end
      end

      context 'invalid' do
        it 'sends the template' do
          stub_request(:post, sendgrid_uri_for_webmock).to_return(
            status: 400, body: "{\"errors\":[{\"field\":\"a\",\"message\":\"message\"}]}", headers: {}
          )
          expect { subject.deliver_now }.to raise_exception(TemplateMailer::Services::BaseService::InvalidRequest)
        end
      end
    end
  end
end
