require 'spec_helper'

RSpec.describe TemplateMailer::ObjectSerializer do
  subject { described_class.new(double) }

  describe '#to_h' do
    let(:double) { instance_double('Obj', foo: :bar) }

    it 'serializes the object' do
      described_class.serializable_fields(:foo)
      expect(subject.to_h).to eq(foo: :bar)
    end

    context 'with custom method' do
      before do
        allow(subject).to receive(:bar).and_return(double)
      end

      it 'serializes the object' do
        described_class.serializable_fields(:foo, :bar)
        expect(subject.to_h).to eq(foo: :bar, bar: double)
      end
    end
  end
end
