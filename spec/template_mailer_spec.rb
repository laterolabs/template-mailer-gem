require 'spec_helper'

RSpec.describe TemplateMailer do
  describe '#config' do
    it 'configures the application' do
      described_class.configure do |config|
        expect(config).to eq described_class
      end
    end
  end
end
