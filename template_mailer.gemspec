require_relative 'lib/template_mailer/version'

Gem::Specification.new do |s|
  s.name          = 'template_mailer'
  s.version       = TemplateMailer::VERSION
  s.authors       = ['Allen Greer']
  s.email         = ['allen@laterolabs.com']

  s.summary       = 'Send emails using dependent mailer services'
  s.description   = 'Write a longer description or delete this line.'
  s.license       = 'MIT'
  s.required_ruby_version = '>= 2.5.0'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  s.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|s|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  s.bindir        = 'exe'
  s.executables   = s.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  s.require_paths = ['lib']

  s.add_development_dependency 'rake', '~> 13.0'
  s.add_development_dependency 'rspec', '~> 3.0'
  s.add_development_dependency 'rubocop', '~> 1'

  s.add_dependency 'activejob', '~> 6'
  s.add_dependency 'activerecord', '~> 6'
  s.add_dependency 'activesupport', '~> 6'
  s.add_dependency 'sendgrid-ruby', '~> 6'
  s.add_dependency 'ruby-handlebars', '0.4.0'
end
